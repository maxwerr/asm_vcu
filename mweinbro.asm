.text

##########################
#
#	$s0 = address of head
#	$s1 = address of tail		
#
#	24 byte nodes
#	0 	= name
#	12 	= money
#	16	= .prev
#	20	= .next
#	
##########################


main:

	jal		headTail
	jal		addNode
	jal		populateNode
	jal		addNode
	jal		populateNode
	
	li		$v0, 4
	la		$a0, menu1
	syscall
	la		$a0, opt1
	syscall
	la		$a0, opt2
	syscall
	la		$a0, opt3
	syscall
	la		$a0, opt4
	syscall
	la		$a0, opt5
	syscall
	
	li		$v0, 5
	syscall
######################################################
#	beq		$v0, 1, nextNode				im here
######################################################
	jr		$ra



test:

	jal 	addNode
	jal		populateNode
	jal		printNode
	j 		end


nextNode:

	la		$a0, 20($a0)
	jr		$ra

prevNode:

	la		$a0, 16($a0)

end:
	li	$v0, 10
	syscall

printNode:

	move 	$t0, $a0

	li		$v0, 4
	la		$a0, 0($t0)
	syscall

	li		$v0, 4
	la		$a0, has
	syscall

	li		$v0, 1
	lw		$a0, 12($t0)
	syscall
	
	jr		$ra


headTail:

	# create the head 
	li		$v0, 9			# allocate memory
	li		$a0, 24			# 24 bytes for 3 slots
	syscall           		# $v0 = address
	move    $s1, $v0    	# $s1 has address of head

	move	$t0, $v0
	
	# create the tail 
	li		$v0, 9      	# allocate memory
	li		$a0, 24			# 24 bytes for 3 slots
	syscall            		# $v0 = address
	move    $s2, $v0    	# $s2 has address of tail

	sw		$s2, 20($s1)	# head.next = tail
	sw		$s1, 16($s2)	# tail.prev = head
	
	move	$a0, $t0
	jr 		$ra				#return to caller




##########################
#
#	$a0 = address of current node
#		
#	$a0 returns address of first block in new node
##########################

addNode:

	# get nodes' addresses
	move	$t0, $a0		# copy caller address
	lw		$t1, 20($a0)	# load caller.next address
	
	# create the new node 
    li		$v0, 9      	# allocate memory
    li		$a0, 24			# 24 bytes for 3 slots
    syscall            		# $v0 = address of added node

	#	t0  ==  v0  == t1
	#	connects new node
	sw		$v0, 20($t0)	# current.next = new
	sw		$t1, 20($v0)	# new.next = current.next
	sw		$v0, 16($t1)	# next.prev = current
	sw		$t0, 16($v0)	# current.prev = current
	

	#return addresses
	move	$a0, $t0		# current
	move	$a1, $v0		# new
	move	$a2, $t1		# next

	jr 		$ra



##########################
#
#	$a0 = address of node to delete
#		
#	
##########################
deleteNode:

	# load registers prev and next
	lw		$t0, 16($a0)	# del.prev
	lw		$t2, 20($a0)	# del.next

	# connect prev and next
	sw		$t2, 20($t0)	# prev.next = del.next
	sw		$t0, 16($t2)	# next.prev = del.prev

	# returns
	move	$a0, $t0		# previous
	move	$a1, $t2		# next


##########################
#
#	$a0 = address of node to populate
#			
#
##########################
populateNode:

	# push $ra onto the stack
	addi	$sp, $sp, -4
	sw		$ra, ($sp)

	# populate the node with Name and Money
	jal		nodeName
	jal		nodeMoney

	# pop $ra and return
	lw		$ra, ($sp)
	addi	$sp, $sp, 4

	jr		$ra


nodeName:
	
	move	$t0, $a0		# hold address of populating node
	
	# new name <= 12 characters
	li		$v0, 54			# input Dialog String
	la		$a1, 0($a0)		# address of input buffer
	la		$a0, nameQ		# message "Whats this member's name?"
	li 		$a2, 12			# take the first 12 characters
	syscall

	move 	$a0, $t0		# return address of populated node
	jr 		$ra		

nodeMoney:

	move	$t0, $a0		# hold address of populating node

	li		$v0, 51			# Input Int Dialog
	la		$a0, moneyQ		# message
	syscall
	sw		$a0, 12($t0)	# store int to Second Block
	
	move	$a0, $t0		# return address of populating node
	jr 		$ra		

	.data

welcome:	.asciiz		"Welcome to XENU5000, your personal Scientology linked list"

moneyQ:		.asciiz		"How much money (in dollars) does this member possess?"
nameQ:		.asciiz		"Whats this member's name?"
memberHas:	.asciiz		"This member has $"

menu1:		.asciiz		"What would you like to do?\n\n"
opt1:		.asciiz		"1-Print Next Member\n"
opt2:		.asciiz		"2-Print Prev Member\n"
opt3:		.asciiz		"3-Delete this Member\n"
opt4:		.asciiz		"4-Add a Member\n"
opt5:		.asciiz		"5-Search for a Member\n"

has:		.asciiz		" has $"
