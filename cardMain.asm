# A Simple Card Game.
#
# main written by P. von Kaenel (not guaranteed to be correct, but it does compile)
#     Note:  students are not to change code in main unless there is a bug.
#
# Remaining procedures written by Maxwell Weinbrown


		.text

main:
		# initialize the game
		# Registers used by main:
		#     $s0: amount player has
		#     $s1: total bet by player
		#     $s2: total points of player's cards
		#     $s3: total points of computer's cards.

		li		$s0, 10				#starting with $10
		
		li		$v0, 30				# syscall: get time
		syscall

		move	$a1, $a0			# put low time bits in $a1
									# need this for next syscall

		li		$a0, 1				# id of number generator
		li		$v0, 40				# syscall: set seed for random num. generator
		syscall

		move	$a0, $s0			# parameter: player's balance
		jal		showBalance

		move	$a0, $s0			# parameter: player's balance
		jal		getBet				# returned in $v0; will not be more than $a0
		move	$s1, $v0			# $s1 = the bet so far
		beq		$s1, $zero, end		# if no bet, end the game
		
		jal		displayRCard		# display a random card (1-5)
									# returned in $v0
		move	$s2, $v0			# $s2 = player's first card

		sub		$a0, $s0, $s1		# parameter: player's balance
		jal		getBet				# returned in $v0
		add		$s1, $s1, $v0		# $s1 = the total bet; will not be more than $s0

		jal		displayRCard		# display card (1-5), return in $v0

		add		$s2, $s2, $v0		# $s2 = sum of player's two cards

		li		$v0, 55				# syscall: MessageDialog
		la		$a0, compTurn
		li		$a1, 1				# information dialog
		syscall
									# Now we draw two cards for the computer
		jal		compCards			# display two cards (each 1-5), return sum in $v0
		move	$s3, $v0			# $s3 = sum of computer's two cards

		move 	$a0, $s2			# player's points
		move	$a1, $s3			# computer's points
		jal		whoWon				# return $v0; 0=computer won, 1=player won

		move	$a0, $v0			# the win code
		move	$a1, $s0			# player's balance
		move	$a2, $s1			# player's bet
		jal		showOutcome			# $v0 = player's new balance

		move	$s0, $v0

		li		$v0, 10				# syscall: terminate program
		syscall

end:

		li		$v0, 10			#syscall: terminate program
		syscall


#---------------------
# showBalance - displays balance in dialog
# paramters:
#		$a0 :	the balance
# return: 
#		none
#---------------------

showBalance:

		li		$v0, 56				# syscall: MessageDialogInt
		add		$a1, $a0, $0
		la		$a0, yourBal
					
		syscall
		jr 		$ra


#---------------------
# getBet - query and return bet
# paramters:
#		$a0 : current balance
# return: 
#		$v0 : bet placed from 0-balance
#---------------------

getBet:

		move 	$t0, $a0
		li		$v0, 51				# syscall: InputDialogInt
		la		$a0, betOffer
		syscall

		blt		$t0, $a0, getBet	# repeat if bet > balance
		add		$v0, $a0, $0
		jr 		$ra
		
		
#---------------------
# displayRCard - shows a random numbered card from 1-5
# paramters:
#		none
# return: 
#		$v0 : random card value 1-5
#---------------------

displayRCard:

		li 		$v0, 42
		li 		$a0, 1
		li		$a1, 4
		syscall
		addi 	$t0, $a0, 1

		li		$v0, 56				# syscall: MessageDialogInt
		la		$a0, firstPlay
		add		$a1, $t0, $0			
		syscall
		
		add		$v0, $t0, $0
		jr		$ra
	
	
#---------------------
# compCards - picks two random cards for computer, displays both
# parameters:
#		none
# return: 
#		$v0 : total value of both cards
#---------------------

compCards:

		li 		$v0, 42
		li 		$a0, 1
		li		$a1, 4
		syscall
		addi 	$t0, $a0, 1

		li 		$v0, 42
		li 		$a0, 1
		li		$a1, 4
		syscall
		addi 	$t1, $a0, 1

		li		$v0, 56				# syscall: MessageDialogInt
		la		$a0, comp1
		add		$a1, $t0, $0			
		syscall

		li		$v0, 56				# syscall: MessageDialogInt
		la		$a0, comp2
		add		$a1, $t1, $0			
		syscall

		add		$v0, $t0, $t1
		jr 		$ra


#---------------------
# whoWon - computes, displays the winner, returns a code for player/cpu
# parameters:
#		$a0 :	players cards
#		$a1 : 	cpus cards
# return: 
#		$v0 :	0 for cpu wins, 1 for player
#---------------------

whoWon:
		
		li		$v0, 0
		bgt		$a0, 6, cpuWin
		li		$v0, 1
		bgt		$a1, 6, playerWin
		li		$v0, 0
		slt		$v0, $a1, $a0
		beq		$v0, 1, playerWin
		j 		cpuWin

#---------------------		
# Used only by whoWon
#---------------------
playerWin:

		move	$t1, $a0			# $t1 contains player score
		move    $t0, $v0			# $t0 contains win code
		li		$v0, 56				# syscall: MessageDialogInt
		la		$a0, playerWinM
		add		$a1, $t1, $0			
		syscall
		move	$v0, $t0
		jr		$ra

#---------------------		
# Used only by whoWon
#---------------------
cpuWin:

		move	$t1, $a1			# $t1 contains cpu score
		move    $t0, $v0			# $t0 contains win code
		li		$v0, 56				# syscall: MessageDialogInt
		la		$a0, cpuWinM
		add		$a1, $t1, $0			
		syscall
		move	$v0, $t0
		jr		$ra


showOutcome:

		addi	$sp, $sp, -4
		sw		$ra, 0($sp)
		beq 	$a0, 1, winsBet
		j		loseBet

winsBet:
		
		jal		showBalance
		add 	$v0, $a1, $a2
		lw		$ra, 0($sp)
		addi	$sp, $sp, 4
		jr		$ra

loseBet:
		
		jal		showBalance
		sub		$v0, $a1, $a2
		lw		$ra, 0($sp)
		addi	$sp, $sp, 4
		jr		$ra
		


#---------------------------------------------------------------
# The data segment.  Students can add more data to this section.

		.data
compTurn:		.asciiz 		"The computer will now be dealt two cards.."
betOffer:		.asciiz			"How much would you like to bet?"
yourBal:		.asciiz			"Your balance is $"
firstPlay:		.asciiz			"Your card is a "
comp1:			.asciiz			"Computer's first card was a "
comp2:			.asciiz			"Computer's second card was a "
playerWinM:		.asciiz			"Good job!  You win with "
cpuWinM:		.asciiz			"Hmm, maybe you should stop gambling... You lost to a "
